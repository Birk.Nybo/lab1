package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    // Game loop
    public void run() {
        while(true) {
            System.out.println("Let's play round " + roundCounter);
            String userMove = userChoice();
            String computerMove = randomChoice();
            String choiceString = ("Human chose " + userMove + ", computer chose " + computerMove + ".");
            
            // check who won
            if (checkWinner(userMove, computerMove)) {
                System.out.println(choiceString + " Human wins!");
                humanScore++;
            } else if (checkWinner(computerMove, userMove)) {
                System.out.println(choiceString + " Computer wins!");
                computerScore++;
            } else {
                System.out.println(choiceString + " It's a tie!");
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            // ask if user wants to play another round
            String continueAnswer = continuePlaying();
            if (continueAnswer.equals("n")) {
                break;
            } else {
                roundCounter++;
            }
        }   
        System.out.println("Bye bye :)");


        //System.out.println("ditt valg var " + userMove);
        
        
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    // generates a random move
    public String randomChoice() {
        Random rand = new Random();
        String cpuChoice = rpsChoices.get(rand.nextInt(rpsChoices.size()));
        return cpuChoice;
    }

     
     
    // asks user for their move
    public String userChoice() {
        while(true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (humanChoice.equals("rock") || humanChoice.equals("paper") || humanChoice.equals("scissors")) {

                return humanChoice;
            } else {
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
            }
        }
    }

    // checks whether the user wants to keep playing
    public String continuePlaying() {
        while(true) {
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (continueAnswer.equals("y") || continueAnswer.equals("n")) {

                return continueAnswer;
            } else {
                System.out.println("I do not understand " + continueAnswer + ". Could you try again?");
            }
        }
    }

    // checks whether user or computer wins the game
    public boolean checkWinner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        } else if (choice1.equals("scissors")) {
            return choice2.equals("paper"); 
        } else {
            return choice2.equals("scissors");
        }
    }
}
